CTP API
=======

The repository here provides the CTP API linux `x86_64` version.

# Processing before commit

We do some setups before commiting API files extracted from official package. And also some of the text files.

### Convert from gb18030 to utf-8

```shell
fd "\.h" -0 | xargs -0 -I "{}" iconv -f gb18030 -t utf-8 -o "~/projects/api-ctp/include/{}" "{}"
```

### Convert end of line from windows to linux

```shell
dos2unix *.h
```

### Remove trailing spaces from header files

```shell
find . -type f -name '*.h' -exec sed --in-place 's/[[:space:]]\+$//' {} \+
```
